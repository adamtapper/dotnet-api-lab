﻿using System;
namespace Calculator.Api.Domain.Responses
{
    public class CalculationResponse
    {
        public double Result { get; set; }
    }
}
