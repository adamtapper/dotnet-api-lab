﻿using System;
using Calculator.Api.Domain.Models;
using Calculator.Api.Domain.Responses;

namespace Calculator.Api.Services
{
    public interface ICalculatorService
    {
        CalculationResponse calculate(CalculationModel vehicle);
    }
}
