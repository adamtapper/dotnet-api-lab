﻿using System;
using Calculator.Api.Domain.Models;
using Calculator.Api.Domain.Responses;

namespace Calculator.Api.Services
{
    public class CalculatorService : ICalculatorService
    {
        public CalculationResponse calculate(CalculationModel vehicle)
        {
            CalculationResponse response = new CalculationResponse();

            switch (vehicle.Operator)
            {
                case "+":
                    response.Result = vehicle.FirstOperand + (double)vehicle.SecondOperand;
                    break;
                case "-":
                    response.Result = vehicle.FirstOperand - (double)vehicle.SecondOperand;
                    break;
                case "÷":
                    if (vehicle.SecondOperand == 0)
                    {
                        response.Result = 0;
                    } else
                    {
                        response.Result = vehicle.FirstOperand / (double)vehicle.SecondOperand;
                    }
                    break;
                case "x":
                    response.Result = vehicle.FirstOperand * (double)vehicle.SecondOperand;
                    break;
                default:
                    Console.WriteLine("No operand found");
                    break;
            }

            return response;
        }
    }
}
