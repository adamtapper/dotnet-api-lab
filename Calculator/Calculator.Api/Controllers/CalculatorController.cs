﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Calculator.Api.Domain.Models;
using Calculator.Api.Domain.Responses;
using Calculator.Api.Services;
using Microsoft.AspNetCore.Mvc;

namespace Calculator.Api.Controllers
{
    public class CalculatorController : Controller
    {
        private readonly ICalculatorService _calculatorService;

        public CalculatorController(ICalculatorService calculatorService)
        {
            _calculatorService = calculatorService;
        }

        [HttpPost]
        [Route("calculator")]
        public ActionResult<CalculationResponse> Post([FromBody] CalculationModel model)
        {
            var result = _calculatorService.calculate(model);

            return StatusCode(200, result);
        }
    }
}
