﻿using System;
using Moq;
using Xunit;
using Calculator.Api.Controllers;
using Calculator.Api.Services;
using Calculator.Api.Domain.Models;
using Calculator.Api.Domain.Responses;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;

namespace BusinessLogicTests.Controllers
{
    public class CalculatorControllerTests
    {
        private Mock<ICalculatorService> mockCalculatorService = new Mock<ICalculatorService>();
        private CalculatorController calculatorController;


        /*
            UNIT TESTS
        */

        [Fact]
        [Trait("Category", "Unit")]
        public void post_calculationModel_calculatorServiceCalled()
        {

            CalculationResponse fakeResponse = new CalculationResponse();
            fakeResponse.Result = 12;

            // Key Concept: Faking using Moq
            mockCalculatorService.Setup(service => service.calculate(It.IsAny<CalculationModel>())).Returns(fakeResponse);
            calculatorController = new CalculatorController(mockCalculatorService.Object);

            // Task: Implement the post call to the controller and assert what the response should be
        }

        /*
            COMPONENT TESTS
        */

        // Key Concept: Unit / Component Test Overlap
        [Theory]
        [InlineData("+", 15)]
        [InlineData("-", 3)]
        [InlineData("x", 54)]
        [InlineData("÷", 1.5)]
        [Trait("Category", "Component")] // Key Concept: Test Grouping
        public void post_validOperator_doubleResultReturned(string symbol, double result)
        {
            // Task: Implement component tests
        }
    }
}
